PROXY_AFP_ENG_20070223_0217.14
None of the men has entered a plea to the charges .
template-id: 528 116 843 62 55 350 843 1653 82 843 62 179
lexeme-id: 1319 28 466 796 1495 1477 1561 797 212 466 1443 84
lex-label: NIL NIL NIL man NIL ARG0 NIL - NIL NIL charge-05 NIL
syncat: N (N\N)/NP NP/N N (S[dcl]\NP)/(S[pt]\NP) (S[pt]\NP)/NP NP/N N/PP PP/NP NP/N N .

PROXY_AFP_ENG_20081104_0091.18
WMD were not the official reason for Denmark entering the war in Iraq .
template-id: 629 350 24 843 175 918 82 210 47 843 62 3205 210 179
lexeme-id: 340 1657 6 466 1 1521 1171 307 399 466 1 1485 631 84
lex-label: mass ARG0 polarity NIL official cause-01 NIL "Denmark" enter-01 NIL war NIL "Iraq" NIL
syncat: N (S[dcl]\NP)/NP (S\NP)\(S\NP) NP/N N/N N/PP PP/NP N (S[ng]\NP)/NP NP/N N ((S\NP)\(S\NP))/NP N .

PROXY_AFP_ENG_20020522_0013.23
Drug trafficking resulted in 55 death sentences in 2001 , 85 in 2000 and 76 in 1999 .
template-id: 2020 62 793 82 2909 UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK 3231 148 179
lexeme-id: 1 958 649 1485 1307 UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK 1485 1504 84
lex-label: drug traffic-00 result-01 NIL quant UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK NIL 1999 NIL
syncat: N/N N (S[dcl]\NP)/PP PP/NP N/N N/N N (N\N)/NP N , N (N\N)/NP N conj N (N\N)/NP N .

PROXY_APW_ENG_20020618_0245.12
10000 people rallied on June 16 , 2002 in Lahore including Aziz .
template-id: 799 62 251 574 353 202 952 894 3205 1166 1625 307 179
lexeme-id: 1761 734 611 299 185 1129 532 505 1485 1467 781 932 84
lex-label: 10000 person rally-01 NIL NIL 16 NIL 2002 NIL "Lahore" NIL "Aziz" NIL
syncat: N/N N S[dcl]\NP ((S\NP)\(S\NP))/NP N/N[num] N[num] , ((S\NP)\(S\NP))\((S\NP)\(S\NP)) ((S\NP)\(S\NP))/NP N (N\N)/NP N .

PROXY_AFP_ENG_20080530_0183.7
The gang has been operating since the late 1990s and has intimate connections with Bulgarian organized crime groups .
template-id: 843 62 55 55 251 2972 843 3861 202 994 664 175 3276 82 438 449 175 62 179
lexeme-id: 466 1 1495 1543 1570 1 466 1 1338 1 1495 1 1530 867 221 228 1 1605 84
lex-label: NIL gang NIL NIL operate-01 since NIL late 1990 and NIL intimate connect-01 NIL "Bulgaria" NIL crime group NIL
syncat: NP/N N (S[dcl]\NP)/(S[pt]\NP) (S[pt]\NP)/(S[ng]\NP) S[ng]\NP ((S\NP)\(S\NP))/NP NP/N N/N N conj (S[dcl]\NP)/NP N/N N/PP PP/NP N/N N/N N/N N .

PROXY_AFP_ENG_20070818_0297.43
In March 2003 Saddam was driven from power by a US-led invasion and fled .
template-id: 42 202 3039 307 55 UNK UNK UNK UNK UNK UNK UNK 994 2447 179
lexeme-id: 1485 1146 1374 530 366 UNK UNK UNK UNK UNK UNK UNK 1 1178 84
lex-label: NIL 3 2003 "Saddam" NIL UNK UNK UNK UNK UNK UNK UNK and NIL NIL
syncat: (S/S)/NP N N\N N (S[dcl]\NP)/(S[pss]\NP) (S[pss]\NP)/PP PP/NP N ((S\NP)\(S\NP))/NP NP/N N/N N conj S[dcl]\NP .

PROXY_AFP_ENG_20050601_0389.1
2005-06-01
template-id: 857
lexeme-id: 472
lex-label: 6
syncat: N

PROXY_AFP_ENG_20030703_0447.2
Sweden -LRB- SE -RRB-
template-id: 210 3129 528 131
lexeme-id: 1072 1433 1185 46
lex-label: "Sweden" NIL NIL NIL
syncat: N LRB N RRB

PROXY_AFP_ENG_20040719_0650.17
Tellez was in the FSLN 's national leadership .
template-id: 162 674 82 843 1956 3125 175 190 179
lexeme-id: 66 366 1485 466 940 1430 1170 95 84
lex-label: name NIL NIL NIL political-party NIL nation lead-02 NIL
syncat: N (S[dcl]\NP)/PP PP/NP NP/N N (NP/(N/PP))\NP N/N N/PP .

PROXY_AFP_ENG_20070511_0288.3
International ; Government ; Telecom ; technology
template-id: 62 2858 2042 2853 62 2853 62
lexeme-id: 1 1265 994 1267 489 1262 1
lex-label: international and ARG0 op3 telecommunication op4 technology
syncat: N ; N ; N ; N

PROXY_APW_ENG_20020618_0245.6
Militants fighting against Indian rule buried the son in the Himalayan forests .
template-id: 506 2173 82 215 62 47 843 62 3205 843 466 62 179
lexeme-id: 1135 1066 1254 115 3 1585 466 1 1485 466 248 831 84
lex-label: militant ARG1 NIL name rule-03 bury-01 NIL son NIL NIL name forest NIL
syncat: N (S[ng]\NP)/PP PP/NP N/N N (S[dcl]\NP)/NP NP/N N/PP ((S\NP)\(S\NP))/NP NP/N N/N N .

PROXY_AFP_ENG_20081029_0096.1
2008-10-29
template-id: 3436
lexeme-id: 1577
lex-label: 29
syncat: N

PROXY_AFP_ENG_20030330_0257.13
There is much concern over the drug .
template-id: 254 664 175 62 1734 843 62 179
lexeme-id: 877 562 1 3 1483 466 1 84
lex-label: NIL NIL much concern-01 NIL NIL drug NIL
syncat: NP[thr] (S[dcl]\NP[thr])/NP N/N N PP/NP NP/N N .

PROXY_AFP_ENG_20020128_0449.6
Russian space agency spokesman Sergei Gorbunov announced --
template-id: 2151 1918 366 718 545 202 1326 3453
lexeme-id: 1050 1 1 1 886 664 654 1588
lex-label: country space agency spokesman "Sergei" "Gorbunov" announce-01 NIL
syncat: N/N ((N/N)/(N/N))/((N/N)/(N/N)) (N/N)/(N/N) N/N N/N N (S[dcl]\S[dcl])\NP :

PROXY_AFP_ENG_20070714_0273.5
Putin demands that all NATO members sign treaty .
template-id: 1750 1214 1938 3105 2378 506 47 62 179
lexeme-id: 832 634 924 1 1157 270 3 1 84
lex-label: name ARG1 NIL all NIL member sign-02 treaty NIL
syncat: N/N (S[dcl]\NP)/S[em] S[em]/S[dcl] NP/N N/N N (S[b]\NP)/NP N .

PROXY_AFP_ENG_20020610_0336.43
Its global announcement comes via the regime 's newly-appointed public relations firm in Washington .
template-id: 452 175 1599 256 82 843 62 3124 3800 3592 3315 62 3233 1166 179
lexeme-id: 1167 1289 763 134 368 466 1 1430 1754 1 1540 1 1485 1282 84
lex-label: NIL globe NIL come-01 NIL NIL regime NIL ARG1 public NIL firm NIL "Washington" NIL
syncat: NP/(N/PP) N/N N/PP (S[dcl]\NP)/PP PP/NP NP/N N (NP/(N/PP))\NP N/N (N/N)/(N/N) N/N N (N\N)/NP N .

PROXY_AFP_ENG_20070209_0566.5
10 other Kurds will go before a judge on 9 February 2007 .
template-id: 799 175 3731 54 251 3570 843 62 574 1593 202 3039 179
lexeme-id: 1349 1 1708 492 3 1 1561 3 299 1051 1119 1389 84
lex-label: 10 other NIL NIL go-01 before NIL judge-01 NIL 9 2 2007 NIL
syncat: N/N N/N N (S[dcl]\NP)/(S[b]\NP) S[b]\NP ((S\NP)\(S\NP))/NP NP/N N (N\N)/NP N/N N/N N\N .

PROXY_AFP_ENG_20071111_0386.14
At least 254 have been hanged in Iran in 2007 .
template-id: 1463 879 202 55 55 250 3205 210 574 148 179
lexeme-id: 682 486 1429 1328 1543 1581 1485 223 1485 1389 84
lex-label: NIL NIL 254 NIL NIL hang-01 NIL "Iran" NIL 2007 NIL
syncat: ((N/N)/(N/N))/(S[asup]\NP) S[asup]\NP N (S[dcl]\NP)/(S[pt]\NP) (S[pt]\NP)/(S[pss]\NP) S[pss]\NP ((S\NP)\(S\NP))/NP N ((S\NP)\(S\NP))/NP N .

PROXY_AFP_ENG_20071219_0023.15
Libya was accused of seeking to acquire weapons of mass destruction by Western governments .
template-id: 210 55 793 99 1319 55 47 1984 82 1150 62 1734 1145 2042 179
lexeme-id: 1724 366 430 28 1532 212 3 948 28 1 50 815 589 1244 84
lex-label: "Libya" NIL accuse-01 NIL seek-01 NIL acquire-01 weapon NIL mass destroy-01 NIL NIL ARG0 NIL
syncat: N (S[dcl]\NP)/(S[pss]\NP) (S[pss]\NP)/PP PP/(S[ng]\NP) (S[ng]\NP)/(S[to]\NP) (S[to]\NP)/(S[b]\NP) (S[b]\NP)/NP N/PP PP/NP N/N N PP/NP N/N N .

PROXY_AFP_ENG_20070428_0521.23
There are believed to be 9 nuclear weapons states .
template-id: 528 55 359 55 664 799 175 175 62 179
lexeme-id: 877 180 188 212 1021 1051 202 948 1424 84
lex-label: NIL NIL NIL NIL NIL 9 nucleus weapon state NIL
syncat: NP[thr] (S[dcl]\NP[thr])/NP (S[pss]\NP)/(S[to]\NP) (S[to]\NP)/(S[b]\NP) (S[b]\NP)/NP N/N N/N N/N N .

PROXY_AFP_ENG_20021102_0031.12
Amnesty urged a prompt and fair investigation by an independent body into the deaths .
template-id: 710 394 843 587 1026 588 3535 82 843 1608 62 82 843 69 179
lexeme-id: 951 208 1561 1 537 1 1612 815 1362 768 1 1068 466 27 84
lex-label: "Amnesty" ARG0 NIL prompt NIL fair NIL NIL NIL ARG0 body NIL NIL die-01 NIL
syncat: N (S[dcl]\NP)/NP NP/N N/N conj N/N N/PP PP/NP NP/N N/N N/PP PP/NP NP/N N .

PROXY_APW_ENG_20020415_0560.26
US withdrawal from the ABM would hurt global stability .
template-id: 531 855 82 843 195 55 47 175 62 179
lexeme-id: 281 469 488 466 100 1350 3 1289 1 84
lex-label: NIL NIL NIL NIL name NIL hurt-01 globe stability NIL
syncat: NP (N/PP)/PP PP/NP NP/N N (S[dcl]\NP)/(S[b]\NP) (S[b]\NP)/NP N/N N .

PROXY_AFP_ENG_20020329_0022.12
The official said it was speculation that the North Korean government thinks the inspections should be delayed until 050000 .
template-id: 843 506 1579 254 664 128 1938 843 1869 931 2042 1571 843 62 3046 55 251 1671 148 179
lexeme-id: 466 1 738 132 366 43 924 466 1059 1696 994 1334 466 1744 1382 1021 655 1 62 84
lex-label: NIL official say-01 NIL NIL NIL NIL NIL "North" "Korea" ARG0 think-01 NIL inspect-01 NIL NIL delay-01 until 2005 NIL
syncat: NP/N N (S[dcl]\NP)/S[dcl] NP (S[dcl]\NP)/NP (S[adj]\NP)/S[em] S[em]/S[dcl] NP/N (N/N)/(N/N) N/N N (S[dcl]\NP)/S[dcl] NP/N N (S[dcl]\NP)/(S[b]\NP) (S[b]\NP)/(S[pss]\NP) S[pss]\NP ((S\NP)\(S\NP))/NP N .

PROXY_AFP_ENG_20080926_0653.3
International ; Government ; weapons ; proliferation
template-id: 62 2858 2042 2853 62 2853 62
lexeme-id: 1 1265 994 1267 948 1262 997
lex-label: international and ARG0 op3 weapon op4 proliferate-01
syncat: N ; N ; N ; N

PROXY_AFP_ENG_20020723_0353.14
The 3 countries share sea borders frequently used by pirates and criminals in evading pursuing troops .
template-id: 843 799 62 47 1548 62 2206 3460 1740 62 994 62 99 137 621 62 179
lexeme-id: 466 436 989 3 1 1736 1079 1593 815 413 1 1039 1485 427 1788 350 84
lex-label: NIL 3 country share-01 sea border-01 frequency use-01 NIL pirate and criminal NIL evade-01 pursue-01 troop NIL
syncat: NP/N N/N N (S[dcl]\NP)/NP N/N N (S\NP)/(S\NP) S[pss]\NP ((S\NP)\(S\NP))/NP N conj N PP/(S[ng]\NP) (S[ng]\NP)/NP (S[ng]\NP)/NP N .

PROXY_AFP_ENG_20050321_0150.12
The technology is highly prized in Taiwan .
template-id: 843 62 55 700 251 3205 210 179
lexeme-id: 466 1 562 386 129 1485 1806 84
lex-label: NIL technology NIL manner prize-01 NIL "Taiwan" NIL
syncat: NP/N N (S[dcl]\NP)/(S[adj]\NP) (S[adj]\NP)/(S[adj]\NP) (S[pss]\NP)/PP ((S\NP)\(S\NP))/NP N .

PROXY_AFP_ENG_20070428_0521.1
2007-04-30
template-id: 653
lexeme-id: 355
lex-label: month
syncat: N

PROXY_AFP_ENG_20030522_0447.11
Al-Zayat stated that the recording was Zawahiri 's voice , style and manner .
template-id: 307 1571 1938 843 62 670 307 3126 246 953 246 1006 246 179
lexeme-id: 164 827 924 466 232 366 566 1430 1 532 1 1 1 84
lex-label: "al-Zayat" state-01 NIL NIL record-01 NIL "Zawahiri" NIL voice NIL style and manner NIL
syncat: N (S[dcl]\NP)/S[em] S[em]/S[dcl] NP/N N (S[dcl]\NP)/NP N (NP/(N/PP))\NP N/PP , N/PP conj N .

PROXY_AFP_ENG_20050322_0158.3
International ; technology ; military
template-id: 62 2858 62 2853 62
lexeme-id: 1 1265 1 1267 1
lex-label: international and technology op3 military
syncat: N ; N ; N

PROXY_APW_ENG_20080415_1054.43
The Brotherhood won 88 of parliament 's 454 seats in the 2005 elections with Brotherhood candidates running as independents .
template-id: 843 710 UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK 3203 3410 179
lexeme-id: 466 394 UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK 1481 1556 84
lex-label: NIL "Brotherhood" UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK NIL - NIL
syncat: NP/N N (S[dcl]\NP)/NP NP/PP PP/NP N (NP/(N/PP))\NP N/N (N/PP)/PP PP/NP NP/N N/N N (N\N)/NP N/N N S[ng]\NP ((S\NP)\(S\NP))/NP N .

PROXY_AFP_ENG_20030220_0697.18
The Iranian government had removed sensitive equipment from the Natanz site in anticipation of the IAEA inspection this weekend .
template-id: 843 222 2042 55 1963 175 62 82 843 185 62 1734 918 82 843 3612 62 2144 62 179
lexeme-id: 466 1360 994 1710 945 1 1 488 466 90 1 1485 1141 28 466 1654 926 1 1 84
lex-label: NIL "Iran" ARG0 NIL NIL sensitive equipment NIL NIL "Natanz" site NIL anticipate-01 NIL NIL "IAEA" inspect-01 this weekend NIL
syncat: NP/N N/N N (S[dcl]\NP)/(S[pt]\NP) ((S[pt]\NP)/PP)/NP N/N N PP/NP NP/N N/N N ((S\NP)\(S\NP))/NP N/PP PP/NP NP/N N/N N ((S\NP)\(S\NP))/N N .

PROXY_AFP_ENG_20051018_0064.11
The daily violence has continued despite a ceasefire declaration in areas affected by the 08 October 2005 earthquake .
template-id: 843 3083 62 55 251 1534 3420 2020 62 3233 62 251 1699 843 1593 931 3771 62 179
lexeme-id: 466 1403 1 1495 296 715 1561 971 1301 1485 75 1772 815 466 894 521 1732 1 84
lex-label: NIL quant violence NIL continue-01 NIL NIL cease-01 declare-02 NIL area affect-01 NIL NIL 8 10 NIL earthquake NIL
syncat: NP/N N/N N (S[dcl]\NP)/(S[pt]\NP) S[pt]\NP ((S\NP)\(S\NP))/NP NP/N N/N N (N\N)/NP N S[pss]\NP ((S\NP)\(S\NP))/NP NP/N N/N N (N/N)\(N/N) N .

PROXY_AFP_ENG_20081021_0061.16
Only the United States voted against .
template-id: 390 843 3709 202 680 2842 179
lexeme-id: 1 466 1690 1420 372 1 84
lex-label: only NIL op1 "States" ARG1 against NIL
syncat: NP/NP NP/N N/N N (S[dcl]\NP)/PP PP/NP .

PROXY_AFP_ENG_20070716_0392.5
On 16 July 2007 an Amnesty International report was released stating --
template-id: 42 1593 202 3039 843 342 2864 62 55 251 3025 3453
lexeme-id: 299 1129 103 1389 1362 951 1271 1 366 623 1365 1588
lex-label: NIL 16 7 2007 NIL "Amnesty" NIL report NIL release-01 state-01 NIL
syncat: (S/S)/NP N/N N N\N NP/N N/N N/N N (S[dcl]\NP)/(S[pss]\NP) S[pss]\NP S[ng]\NP :

PROXY_AFP_ENG_20070302_0535.15
Iran is facing new United Nations sanctions after refusing to stop enriching uranium
template-id: 210 55 137 175 3689 931 62 3336 1319 55 1319 47 62
lexeme-id: 223 562 792 1 1690 636 603 1 662 212 3 1251 1
lex-label: "Iran" NIL face-01 new op1 "Nations" sanction-02 after refuse-01 NIL stop-01 enrich-01 uranium
syncat: N (S[dcl]\NP)/(S[ng]\NP) (S[ng]\NP)/NP N/N (N/N)/(N/N) N/N N ((S\NP)\(S\NP))/(S[ng]\NP) (S[ng]\NP)/(S[to]\NP) (S[to]\NP)/(S[b]\NP) (S[b]\NP)/(S[ng]\NP) (S[ng]\NP)/NP N

PROXY_AFP_ENG_20070415_0167.13
Israel is the only nuclear power in the region despite the program being undeclared .
template-id: 210 664 843 175 175 62 3233 843 62 1522 843 62 55 250 179
lexeme-id: 1024 562 466 1 202 1 1485 466 1 715 466 1 18 895 84
lex-label: "Israel" NIL NIL only nucleus power NIL NIL region NIL NIL program NIL declare-02 NIL
syncat: N (S[dcl]\NP)/NP NP/N N/N N/N N (N\N)/NP NP/N N/PP ((S\NP)\(S\NP))/NP NP/N N (S[ng]\NP)/(S[pss]\NP) S[pss]\NP .

PROXY_AFP_ENG_20020111_0093.27
The group said the foreign broadcasters do not show any remorse for their culture of eating snails and horses .
template-id: 843 62 1571 843 2392 1138 55 27 137 2028 638 82 452 3139 99 137 62 994 62 179
lexeme-id: 466 1 738 466 1 582 256 6 3 1 1 1171 233 1 28 1524 1742 1 753 84
lex-label: NIL group say-01 NIL foreign broadcast-01 NIL polarity show-01 any remorse NIL NIL culture NIL eat-01 snail and horse NIL
syncat: NP/N N (S[dcl]\NP)/S[dcl] NP/N N/N N (S[dcl]\NP)/(S[b]\NP) (S\NP)\(S\NP) (S[b]\NP)/NP NP/N N/PP PP/NP NP/(N/PP) (N/PP)/PP PP/(S[ng]\NP) (S[ng]\NP)/NP N conj N .

PROXY_AFP_ENG_20070326_0201.10
The resolution was passed unanimously on 070324 after days of hard bargaining by the Security Council 's 15 members .
template-id: 843 62 55 251 683 574 298 1671 804 72 587 62 1734 843 818 202 3125 799 498 179
lexeme-id: 466 1 366 762 374 299 153 1 441 28 1 668 815 466 448 870 1430 1449 270 84
lex-label: NIL resolution NIL pass-01 mod NIL year after NIL NIL hard bargain-01 NIL NIL op1 "Council" NIL 15 member NIL
syncat: NP/N N (S[dcl]\NP)/(S[pss]\NP) S[pss]\NP (S\NP)\(S\NP) ((S\NP)\(S\NP))/NP N ((S\NP)\(S\NP))/NP N/PP PP/NP N/N N ((S\NP)\(S\NP))/NP NP/N N/N N (NP/(N/PP))\NP N/N N/PP .

PROXY_AFP_ENG_20070521_0178.22
Imposing temporary restrictions on Internet traffic from abroad was inevitable .
template-id: UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK
lexeme-id: UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK
lex-label: UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK
syncat: (S[ng]\NP)/NP N/N N/PP PP/NP N/N N ((S\NP)\(S\NP))/NP N (S[dcl]\NP)/(S[adj]\NP) S[adj]\NP .

PROXY_AFP_ENG_20050708_0783.10
The agreement guarantees access to Meltrex by Brazilian patients .
template-id: 843 62 47 918 82 2112 1734 438 62 179
lexeme-id: 466 1427 107 3 212 1015 815 516 969 84
lex-label: NIL agree-01 guarantee-01 access-01 NIL NIL NIL "Brazil" patient NIL
syncat: NP/N N/N (S[dcl]\NP)/NP N/PP PP/NP N ((S\NP)\(S\NP))/NP N/N N .

PROXY_AFP_ENG_20071211_0449.5
Iran is the most prolific applier of the death penalty in the world after China .
template-id: 210 1111 843 3167 175 722 82 843 889 62 3233 843 62 3334 210 179
lexeme-id: 223 562 466 1 1 407 28 466 500 1385 1485 466 1 1 677 84
lex-label: "Iran" NIL NIL most prolific NIL NIL NIL NIL penalize-01 NIL NIL world after "China" NIL
syncat: N (S[dcl]\NP)/NP NP/N (N/N)/(N/N) N/N N/PP PP/NP NP/N N/N N (N\N)/NP NP/N N (N\N)/NP N .

PROXY_AFP_ENG_20071109_0396.3
International ; Telecom ; technology ; crime ; Government ; politics
template-id: 62 2858 62 UNK UNK UNK UNK UNK UNK UNK UNK
lexeme-id: 1 1265 489 UNK UNK UNK UNK UNK UNK UNK UNK
lex-label: international and telecommunication UNK UNK UNK UNK UNK UNK UNK UNK
syncat: N ; N ; N ; N ; N ; N

PROXY_AFP_ENG_20081119_0042.16
The Costa Rican President decried the limited application of article 26 of the UN Charter .
template-id: 843 1875 931 506 47 843 141 918 82 246 3192 82 843 3737 202 179
lexeme-id: 466 904 1106 1 13 466 56 511 28 1 1475 28 466 1712 1233 84
lex-label: NIL NIL "Rica" president decry-01 NIL ARG1 apply-02 NIL article NIL NIL NIL NIL "Charter" NIL
syncat: NP/N (N/N)/(N/N) N/N N (S[dcl]\NP)/NP NP/N N/N N/PP PP/NP N/PP NP/PP PP/NP NP/N N/N N .

PROXY_AFP_ENG_20080530_0415.3
Crime ; terrorism
template-id: 62 2858 62
lexeme-id: 1 1265 1
lex-label: crime and terrorism
syncat: N ; N

PROXY_AFP_ENG_20070504_0296.10
Municipal elections are scheduled for 070513 and 070514 in Corleone .
template-id: UNK UNK 55 2965 UNK UNK UNK UNK 82 1166 179
lexeme-id: UNK UNK 180 1312 UNK UNK UNK UNK 1485 1341 84
lex-label: UNK UNK NIL schedule-01 UNK UNK UNK UNK NIL "Corleone" NIL
syncat: N/N N (S[dcl]\NP)/(S[pss]\NP) (S[pss]\NP)/PP PP/NP N conj N ((S\NP)\(S\NP))/NP N .

PROXY_AFP_ENG_20071003_0237.17
Shivraj Patil stated that terror attacks could affect foreign investment .
template-id: 545 202 1571 1938 175 62 1559 47 2394 62 179
lexeme-id: 289 964 827 924 1 391 729 3 1 1176 84
lex-label: "Shivraj" "Patil" state-01 NIL terror attack-01 possible affect-01 foreign invest-01 NIL
syncat: N/N N (S[dcl]\NP)/S[em] S[em]/S[dcl] N/N N (S[dcl]\NP)/(S[b]\NP) (S[b]\NP)/NP N/N N .

PROXY_AFP_ENG_20071007_0248.4
Afghan authorities raided a drugs laboratory and a customs office on 6 October 2007 .
template-id: 438 62 47 843 175 62 994 843 175 62 574 1593 202 3039 179
lexeme-id: 1097 1370 52 1561 1782 1 1 1561 1 1 299 756 521 1389 84
lex-label: "Afghanistan" authority raid-01 NIL drug laboratory and NIL customs office NIL 6 10 2007 NIL
syncat: N/N N (S[dcl]\NP)/NP NP/N N/N N conj NP/N N/N N (N\N)/NP N/N N/N N\N .

PROXY_AFP_ENG_20021230_0032.39
Only the US and former USSR have sent humans into space .
template-id: 383 843 525 994 175 210 55 1929 62 82 62 179
lexeme-id: 1 466 281 1 1 805 1328 919 459 1068 1 84
lex-label: only NIL NIL and former "USSR" NIL ARG0 human NIL space NIL
syncat: NP/NP NP/N N conj N/N N (S[dcl]\NP)/(S[pt]\NP) ((S[pt]\NP)/PP)/NP N PP/NP N .

PROXY_AFP_ENG_20071003_0237.16
A top intelligence official stated that various other agencies chasing ghost investors in stocks have given similar warnings .
template-id: UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK
lexeme-id: UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK
lex-label: UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK UNK
syncat: NP/N N/N N/N N (S[dcl]\NP)/S[em] S[em]/S[dcl] N/N N/N N (S[ng]\NP)/NP N N (N\N)/NP N (S[dcl]\NP)/(S[pt]\NP) ((S\NP)\(S\NP))/NP N/N N .

PROXY_AFP_ENG_20081119_0042.11
Arias said Costa Rica is not a naive nation even though it has no military .
template-id: 307 1576 1882 202 664 24 843 175 62 3849 3867 254 UNK UNK UNK UNK
lexeme-id: 620 738 904 1767 562 6 1561 1 1 1793 1801 132 UNK UNK UNK UNK
lex-label: "Arias" say-01 NIL "Rica" NIL polarity NIL naive nation NIL NIL NIL UNK UNK UNK UNK
syncat: N (S[dcl]\NP)/S[dcl] N/N N (S[dcl]\NP)/NP (S\NP)\(S\NP) NP/N N/N N ((S\NP)\(S\NP))/((S\NP)\(S\NP)) ((S\NP)\(S\NP))/S[dcl] NP (S[dcl]\NP)/NP NP/N N .

PROXY_AFP_ENG_20070818_0297.23
Raghad has an aggressive temperament .
template-id: 307 47 843 175 62 179
lexeme-id: 378 1500 1362 1 1 84
lex-label: "Raghad" have-03 NIL aggressive temperament NIL
syncat: N (S[dcl]\NP)/NP NP/N N/N N .

PROXY_AFP_ENG_20071201_0434.3
Narcotics ; crime
template-id: 62 2858 62
lexeme-id: 1084 1265 1
lex-label: narcotic and crime
syncat: N ; N

PROXY_APW_ENG_20030406_0627.28
During the attack planning an Indonesian and a Kuwaiti arrived in Singapore with instructions for the embassy operations .
template-id: 42 843 2020 62 843 282 994 843 282 1613 82 210 1818 866 82 843 2020 62 179
lexeme-id: 9 466 3 23 1362 238 1 1561 150 776 1485 1259 867 481 1171 466 1 194 84
lex-label: NIL NIL attack-01 plan-01 NIL "Indonesia" and NIL "Kuwait" ARG4 NIL "Singapore" NIL instruction NIL NIL embassy operate-01 NIL
syncat: (S/S)/NP NP/N N (S[ng]\NP)/NP NP/N N conj NP/N N (S[dcl]\NP)/PP ((S\NP)\(S\NP))/NP N ((S\NP)\(S\NP))/NP N/PP PP/NP NP/N N/N N .

PROXY_APW_ENG_20020415_0560.1
2002-04-15
template-id: 3601
lexeme-id: 1642
lex-label: 4
syncat: N

PROXY_APW_ENG_20080304_0925.6
Francisco Santos stated that Colombian police found the evidence on two computers discovered with Raul Reyes .
template-id: 545 202 1571 1938 438 62 47 843 2989 82 799 62 242 82 545 202 179
lexeme-id: 596 251 827 924 1510 1 1720 466 3 299 774 809 121 867 319 1658 84
lex-label: "Francisco" "Santos" state-01 NIL "Colombia" police find-01 NIL evidence-01 NIL 2 computer discover-01 NIL "Raul" "Reyes" NIL
syncat: N/N N (S[dcl]\NP)/S[em] S[em]/S[dcl] N/N N (S[dcl]\NP)/NP NP/N N PP/NP N/N N (S[pss]\NP)/PP ((S\NP)\(S\NP))/NP N/N N .

PROXY_AFP_ENG_20020210_0074.17
Opium and heroin are a form of hard-currency reserve for Afghanistan .
template-id: 62 994 62 350 843 662 82 175 646 82 210 179
lexeme-id: 1 1 1 182 1561 1 28 572 3 1171 1748 84
lex-label: opium and heroin domain NIL form NIL hard reserve-01 NIL "Afghanistan" NIL
syncat: N/N conj N (S[dcl]\NP)/NP NP/N N/PP PP/NP N/N N/PP ((S\NP)\(S\NP))/NP N .

PROXY_AFP_ENG_20070620_0032.8
The New South Wales Jewish Board of Deputies stated that Bethlehem 's Council was controlled by Hamas .
template-id: 843 342 3944 2256 931 202 76 202 1571 1938 1166 3124 62 55 250 1734 710 179
lexeme-id: 466 177 1810 1109 1681 1464 28 1790 827 924 1298 1430 1 366 1151 815 419 84
lex-label: NIL "New" op2 "Wales" "Jewish" "Board" NIL "Deputies" state-01 NIL "Bethlehem" NIL council NIL control-01 NIL "Hamas" NIL
syncat: NP/N N/N ((N/N)/(N/N))/((N/N)/(N/N)) N/N N/N N (N\N)/NP N (S[dcl]\NP)/S[em] S[em]/S[dcl] N (NP/(N/PP))\NP N (S[dcl]\NP)/(S[pss]\NP) S[pss]\NP ((S\NP)\(S\NP))/NP N .

PROXY_AFP_ENG_20070215_0113.10
Narayanan first made the comments at a high-level gathering on international security in Munich on 11 February 2007 .
template-id: 307 262 350 843 3839 82 843 175 2812 563 175 62 3233 1166 574 2456 202 3037 179
lexeme-id: 1029 139 1590 466 1783 682 1561 1411 1248 299 1 1 1485 605 299 1183 1119 1389 84
lex-label: "Narayanan" ordinal-entity ARG0 NIL NIL NIL NIL high mod NIL international security NIL "Munich" NIL 11 2 2007 NIL
syncat: N (S\NP)/(S\NP) (S[dcl]\NP)/NP NP/N N/PP PP/NP NP/N N/N N/PP (N\N)/NP N/N N (N\N)/NP N ((S\NP)\(S\NP))/NP N/N N/N N\N .

PROXY_AFP_ENG_20020723_0353.18
The trilateral pact between Malaysia , Indonesia and the Philippines constituted a constructive first step .
template-id: 843 175 62 760 210 955 210 985 843 210 47 843 175 266 62 179
lexeme-id: 466 1 1 421 168 532 457 537 466 785 497 1561 1 139 1 84
lex-label: NIL trilateral pact have-org-role-91 "Malaysia" NIL "Indonesia" NIL NIL "Philippines" constitute-01 NIL constructive ordinal-entity step NIL
syncat: NP/N N/N N/PP PP/NP N , N conj NP/N N (S[dcl]\NP)/NP NP/N N/N N/N N .

PROXY_AFP_ENG_20020111_0093.3
International ; politics ; Telecom
template-id: 62 2858 62 2853 62
lexeme-id: 1 1265 1 1267 1
lex-label: international and politics op3 telecom
syncat: N ; N ; N

PROXY_AFP_ENG_20070716_0508.27
Indian does not have an arms embargo against Myanmar .
template-id: 210 55 24 1111 843 2020 2221 82 210 179
lexeme-id: 112 1507 6 1328 1362 1821 3 1254 847 84
lex-label: "India" NIL polarity NIL NIL arm embargo-01 NIL "Myanmar" NIL
syncat: N (S[dcl]\NP)/(S[b]\NP) (S\NP)\(S\NP) (S[b]\NP)/NP NP/N N/N N/PP PP/NP N .

PROXY_AFP_ENG_20070716_0508.23
Another Indian official stated that the technology was for communications only and not for offensive operations .
template-id: 2028 226 506 1571 1938 843 62 673 82 3496 62 994 28 82 620 62 179
lexeme-id: 1 115 1 827 924 466 1 366 1171 1598 1 1 6 1171 327 197 84
lex-label: another name official state-01 NIL NIL technology NIL NIL communicate-01 only and polarity NIL offend-01 operation NIL
syncat: NP/N N/N N (S[dcl]\NP)/S[em] S[em]/S[dcl] NP/N N (S[dcl]\NP)/PP PP/NP N (S\NP)\(S\NP) conj (S\NP)\(S\NP) PP/NP N/N N .

PROXY_AFP_ENG_20040424_0639.11
DIHK President Ludwig Georg Braun advised companies not to wait for better policies .
template-id: 2339 718 541 3134 3142 3979 62 25 55 3540 82 3043 62 179
lexeme-id: 1121 1 1088 1434 1444 1827 21 6 212 3 1171 1378 1144 84
lex-label: name president "Ludwig" op2 name ARG0 company polarity NIL wait-01 NIL good policy NIL
syncat: (N/N)/(N/N) N/N N/N N/N N ((S[dcl]\NP)/(S[to]\NP))/NP N (S\NP)/(S\NP) (S[to]\NP)/(S[b]\NP) (S[b]\NP)/PP ((S\NP)\(S\NP))/NP N/N N .

PROXY_AFP_ENG_20020423_0076.2
Republic of Korea -LRB- KR -RRB- ; United States -LRB- US -RRB-
template-id: 202 109 202 3129 528 131 2858 3709 202 116 528 131
lexeme-id: 1677 28 1609 1433 751 46 1265 1690 1420 1433 281 46
lex-label: "Republic" NIL "Korea" NIL NIL NIL and op1 "States" NIL NIL NIL
syncat: N (N\N)/NP N (NP\NP)/NP N RRB ; N/N N (N\N)/NP N RRB

