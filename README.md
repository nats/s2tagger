# s2tagger, a simple supertagger

s2tagger is a tool for predicting tag sequences, implemented by a recurrent neural network.

Its core features are:

- Training labels may be gappy. If the correct tag for a given token is unknown, simply label it as `UNK`.
- Downstream tasks may need to work with realistic predicted tags instead of gold standard tags. s2tagger provides
  the `jackknife` command that predicts tags for the train set by training on *n-1* out of *n* parts of it in turn. 
