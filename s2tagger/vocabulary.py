from collections import Counter

import numpy as np
from tensorflow.keras.preprocessing.sequence import pad_sequences


class Vocabulary:
    def __init__(self, sequences, unknown='UNK', min_count=1):
        self.unknown = unknown
        token_counts = Counter(t for s in sequences for t in s)
        self.token_set = {t for t, count in token_counts.items() if count >= min_count}
        self.token_list = ['PAD']  # index 0 is padding and masked by the network
        if unknown:
            self.token_list.append(unknown)
            if unknown in self.token_set:
                self.token_set.remove(unknown)
        self.token_list.extend(list(self.token_set))
        self.token_index = {t: i for i, t in enumerate(self.token_list)}

    def size(self):
        return len(self.token_list)

    def tok2idx(self, t):
        if t not in self.token_index:
            return self.token_index[self.unknown]
        return self.token_index[t]

    def idx2tok(self, i):
        return self.token_list[i]

    def transform_and_pad(self, sequences, max_len):
        transformed = [[self.tok2idx(t) for t in s] for s in sequences]
        padded = pad_sequences(maxlen=max_len, sequences=transformed, padding='post')
        return padded


class GloveVocabulary:
    instance = None

    def __init__(self, glove_file, pad='PAD'):
        self.pad = pad
        self._load(glove_file)

    def _load(self, file_name):
        print('Loading GloVe embeddings')

        vectors = []
        self.token_list = []
        self.token_index = {}

        with open(file_name) as f:
            for line in f:
                values = line.split()
                word = values[0]
                coefs = np.asarray(values[1:], dtype='float32')
                vectors.append(coefs)
                self.token_index[word] = len(self.token_list)
                self.token_list.append(word)

        self.embedding_matrix = np.array(vectors)

    def tok2idx(self, t):
        t = t.lower()
        if t not in self.token_index:
            return self.token_index['unk']
        return self.token_index[t]

    def idx2tok(self, i):
        return self.token_list[i]

    def transform_and_pad(self, sequences, max_len):
        transformed = [[self.tok2idx(t) for t in s] for s in sequences]
        padded = pad_sequences(maxlen=max_len, sequences=transformed, padding='post', value=self.tok2idx(self.pad))
        return padded

    @staticmethod
    def get(glove_file, pad='PAD'):
        if GloveVocabulary.instance is None:
            GloveVocabulary.instance = GloveVocabulary(glove_file, pad)
        return GloveVocabulary.instance
