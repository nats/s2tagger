"""
Implementation follows the guide:
https://www.depends-on-the-definition.com/guide-sequence-tagging-neural-networks-python/

Possible extension: LSTM-CRF
https://www.depends-on-the-definition.com/sequence-tagging-lstm-crf/
"""
import json
import os
import pickle
import sys
from collections import Counter
import numpy as np

from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Model
from tensorflow.keras.layers import LSTM, Dropout, Bidirectional, Concatenate

from . import limit_predictions
from .masked_loss import masked_categorical_crossentropy
from .metrics import top_k_accuracy
from .prediction import Prediction
from .tagger_config import TaggerConfig
from .util.numpy_json_encoder import NumpyJsonEncoder

TEMPLATE_LAYER = 0
LEXEME_LAYER = 1


def calculate_rank_recalls(val, pred):
    ranks = Counter(rank for rank_seq in pred.val_ranks(val, exclude='UNK') for rank in rank_seq)
    total_toks = sum(len([t for t in s if t != 'UNK']) for s in val)

    # Summarize ranks up to but not including i
    def summarize_rank(i):
        return sum(ranks[j] for j in range(i)) / total_toks

    tmpl_rank_summaries = [summarize_rank(i+1) for i in range(10)]
    return tmpl_rank_summaries


class Tagger:
    def __init__(self, config, tagger_inputs=None, tagger_outputs=None, model=None):
        self.config = config
        self.tagger_inputs = tagger_inputs
        self.tagger_outputs = tagger_outputs
        self.model = model

        self.max_sentence_length = config['max_sentence_length']
        self.epochs = config['epochs']
        self.rnn_dim = config['rnn_dim']
        self.rnn_layers = config['rnn_layers']
        self.feature_layers = config['features']['feature_layers']
        self.predict_layers = config['predict_layers']
        self.predict_layer_count = len(self.predict_layers)
        self.max_predictions = self.config['max_predictions'] if 'max_predictions' in self.config else 10

        self.history = None

    @staticmethod
    def load(path):
        config = TaggerConfig.load(os.path.join(path, 'config.yaml'))

        with open(os.path.join(path, 'tagger_inputs.pkl'), 'rb') as f:
            tagger_inputs = pickle.load(f)
        with open(os.path.join(path, 'tagger_outputs.pkl'), 'rb') as f:
            tagger_outputs = pickle.load(f)

        tagger = Tagger(config, tagger_inputs=tagger_inputs, tagger_outputs=tagger_outputs)

        tagger._init_model()
        tagger.model.load_weights(os.path.join(path, 'weights.h5'))

        return tagger

    def save(self, path):
        os.makedirs(path, exist_ok=True)
        self.config.save(os.path.join(path, 'config.yaml'))
        self.model.save_weights(os.path.join(path, 'weights.h5'))
        with open(os.path.join(path, 'tagger_inputs.pkl'), 'wb') as f:
            pickle.dump(self.tagger_inputs, f)
        with open(os.path.join(path, 'tagger_outputs.pkl'), 'wb') as f:
            pickle.dump(self.tagger_outputs, f)
        with open(os.path.join(path, 'history.json'), 'w') as f:
            json.dump(self.history.history, f, indent=2, cls=NumpyJsonEncoder)

    def predict(self, data_set):
        x = self.extract_x(data_set)
        preds = self.model.predict(x, batch_size=32)

        predictions = []
        if self.predict_layer_count > 1:
            for i, tagger_output in enumerate(self.tagger_outputs):
                predictions.append(Prediction.from_model_prediction(preds[i], tagger_output.vocab, tagger_output.layer_name))
        else:
            tagger_output = self.tagger_outputs[0]
            predictions.append(Prediction.from_model_prediction(preds, tagger_output.vocab, tagger_output.layer_name))

        def limit(p):
            return limit_predictions.limit(p, min_confidence=self.config['min_confidence'],
                                           clip_confidence=self.config['clip_confidence'],
                                           max_predictions=self.config['max_predictions'])

        limited = [limit(p) for p in predictions]
        return limited

    def extract_x(self, data_set):
        x = []
        for tagger_input in self.tagger_inputs:
            x.append(tagger_input.extract(data_set))
        return x

    def extract_y(self, data_set):
        y = []
        for tagger_output in self.tagger_outputs:
            y.append(tagger_output.extract(data_set))

        for (output, data) in zip(self.tagger_outputs, y):
            unk = output.vocab.tok2idx('UNK')
            pad = output.vocab.tok2idx('PAD')
            decoded = np.argmax(data, axis=2)
            total = np.size(decoded)
            pad_count = np.sum(np.equal(decoded, pad))
            unk_count = np.sum(np.equal(decoded, unk))
            other_count = total - pad_count - unk_count
            print(f'{output.layer_name} pad: {pad_count / total}, unk: {unk_count / total}, other: {other_count / total}', file=sys.stderr)

        return y

    def train(self, train_data, val_data=None):
        self._init_vocabs(train_data)
        self._init_model()

        x_train = self.extract_x(train_data)
        y_train = self.extract_y(train_data)

        validation_data = None
        if val_data:
            x_val = self.extract_x(val_data)
            y_val = self.extract_y(val_data)
            validation_data = x_val, y_val

        callbacks = [EarlyStopping(monitor='val_top{}'.format(self.max_predictions), mode='max', patience=10, restore_best_weights=True)]

        history = self.model.fit(x_train, y_train, batch_size=32, epochs=self.epochs, validation_data=validation_data,
                                 callbacks=callbacks, verbose=2)
        self.history = history

    def _init_vocabs(self, train_data):
        """
        Creates vocabularies from a train data set and loads auxiliary resources such as word embeddings.

        :param train_data: Training data set to extract vocabularies from
        """
        self.tagger_inputs = self.config.create_tagger_inputs(train_data)
        self.tagger_outputs = self.config.create_tagger_outputs(train_data)

    def _init_model(self):
        """
        Builds the Keras model.
        """

        inputs = []
        embedded_inputs = []

        # Create inputs and embed them
        for tagger_input in self.tagger_inputs:
            inp, embedded = tagger_input.embed()
            inputs.append(inp)
            embedded_inputs.append(embedded)

        if len(embedded_inputs) > 1:
            inputs_combined = Concatenate(axis=2)(embedded_inputs)
        else:
            inputs_combined = embedded_inputs[0]

        inputs_combined = Dropout(0.1)(inputs_combined)

        rnn = inputs_combined
        # The core of the model: a multi-layer BiLSTM
        for _ in range(self.rnn_layers):
            rnn = Bidirectional(LSTM(units=self.rnn_dim, return_sequences=True, recurrent_dropout=0.1))(rnn)

        # Create output layers
        outputs = []
        for tagger_output in self.tagger_outputs:
            out_layer = tagger_output.output_layer()(rnn)
            outputs.append(out_layer)

        model = Model(inputs=inputs, outputs=outputs)

        # Create losses
        losses = []
        for tagger_output in self.tagger_outputs:
            mask_value = to_categorical(tagger_output.vocab.tok2idx('UNK'), num_classes=tagger_output.vocab.size())
            losses.append(masked_categorical_crossentropy(mask_value))

        metrics = {}
        for tagger_output in self.tagger_outputs:
            # Mask both UNK and PAD tokens for evaluation
            mask_values = [self.tagger_outputs[0].vocab.tok2idx('UNK'), self.tagger_outputs[0].vocab.tok2idx('PAD')]
            metrics[tagger_output.output_layer_name] = [
                top_k_accuracy(1, mask_values),
                top_k_accuracy(self.max_predictions, mask_values)
            ]

        model.compile(optimizer="adam", loss=losses, metrics=metrics)
        self.model = model
