import copy
import h5py
from typing import Dict, List, Iterable, Any

from .tagger_config import TaggerConfig
from .vocabulary import Vocabulary


class Example:
    def __init__(self, example_id: str, sentence: List[str], layers: Dict[str, Iterable[Any]]):
        self.id = example_id
        self.sentence = sentence
        self.layers = layers

    def bare(self):
        """
        Remove all layers.

        :return: an Example without layers
        """
        return Example(self.id, self.sentence, {})

    def add_layer(self, layer, name):
        new_layers = copy.copy(self.layers)
        new_layers[name] = layer
        return Example(self.id, self.sentence, new_layers)

    @staticmethod
    def from_chunk(chunk):
        example_id = chunk[0]
        sentence = chunk[1].split(' ')
        layers = {}
        for l in chunk[2:]:
            name, tags = l.split(": ", maxsplit=1)
            layers[name] = tags.split(' ')
        return Example(example_id, sentence, layers)


class DataSet:
    def __init__(self, examples: List[Example]):
        self.examples = examples
        self.layer_count = max(len(e.layers) for e in examples) if self.examples else 0

    def size(self):
        return len(self.examples)

    def sentences(self):
        return [example.sentence for example in self.examples]

    def layer(self, layer):
        return [example.layers[layer] for example in self.examples]

    def make_layer_vocabs(self, min_count=2):
        return {l: Vocabulary(self.layer(l), min_count=min_count) for l in self.examples[0].layers}

    def split(self, proportion):
        split_point = int(proportion * len(self.examples))
        return DataSet(self.examples[:split_point]), DataSet(self.examples[split_point:])

    def n_way_split(self, n_splits):
        """
        Split the data into n (roughly) equally sized portions.

        :param n_splits: number of portions to create
        :return: a list of DataSets
        """
        split_proportion = 1.0 / n_splits
        indices = [int(split_proportion * i * self.size()) for i in range(0, n_splits + 1)]
        slices = [self.examples[start:end] for start, end in zip(indices, indices[1:])]
        return [DataSet(sl) for sl in slices]

    def bare(self):
        """
        Remove all layers.

        :return: a DataSet without layers
        """
        return DataSet([e.bare() for e in self.examples])

    def add_layer(self, layer, name=None):
        return DataSet([e.add_layer(seq, name) for e, seq in zip(self.examples, layer)])

    def save(self, file):
        """
        Save as a chunked file with one line per layer that can be loaded again.

        :param file: the file to write to
        :return: None
        """
        for example in self.examples:
            file.write(example.id + '\n')
            file.write(' '.join(example.sentence) + '\n')
            for layer in example.layers:
                file.write(' '.join(layer) + '\n')
            file.write('\n')

    def save_attribute_file(self, file):
        """
        Save as an attribute file that can be loaded by gramr.

        :param file: the file to write to
        :return: None
        """
        for example in self.examples:
            file.write('# ::id {}\n'.format(example.id))
            for layer_name, layer in example.layers.items():
                file.write('# ::supertags-{} {}\n'.format(layer_name, ' '.join(layer)))
            file.write('\n')

    @staticmethod
    def combine(data_sets):
        return DataSet([e for ds in data_sets for e in ds.examples])


def load_seq_file(file_name, lower=False):
    with open(file_name) as f:
        lines = (l.strip() for l in f.readlines())
        if lower:
            lines = (l.lower() for l in lines)
        seqs = [l.split(' ') for l in lines]
        return [s for s in seqs if s]  # filter empty lines


def load_layer_file(file) -> List[Example]:
    lines = [l.strip() for l in file.readlines()]
    chunks = []
    current_chunk = []
    for l in lines:
        if not l and current_chunk:
            chunks.append(current_chunk)
            current_chunk = []
        else:
            current_chunk.append(l)
    if current_chunk:
        chunks.append(current_chunk)

    examples = [Example.from_chunk(chunk) for chunk in chunks]
    return examples


def load_data_set(config: TaggerConfig, name: str) -> DataSet:
    data_set_config = config['data'][name]
    layer_file = config.resolve_file(data_set_config['features'])
    with open(layer_file) as f:
        examples = load_layer_file(f)

    if 'elmo' in data_set_config:
        id_file_name = config.resolve_file(data_set_config['elmo']['ids'])
        vector_file_name = config.resolve_file(data_set_config['elmo']['vectors'])
        with open(id_file_name) as id_file:
            ids = {l.strip(): i for i, l in enumerate(id_file.readlines())}
        h5py_file = h5py.File(vector_file_name, 'r')

        def add_embedding_layer(example):
            embedding = h5py_file.get(str(ids[example.id]))
            return example.add_layer(embedding[2], 'elmo')  # Load only the topmost embedding layer

        examples = [add_embedding_layer(example) for example in examples]

    return DataSet(examples)
