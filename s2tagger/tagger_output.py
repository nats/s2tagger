from tensorflow.keras.layers import TimeDistributed, Dense
from tensorflow.keras.utils import to_categorical
import numpy as np

from .vocabulary import Vocabulary


class TaggerOutput:
    def __init__(self, data_set, layer_name, max_sentence_length: int, min_count: int = 2):
        self.layer_name = layer_name
        self.output_layer_name = 'out_{}'.format(self.layer_name)
        self.max_sentence_length = max_sentence_length
        self.vocab = Vocabulary(data_set.layer(layer_name), min_count=min_count)

    def output_layer(self):
        return TimeDistributed(Dense(self.vocab.size(), activation="softmax"), name=self.output_layer_name)

    def extract(self, data_set):
        layer_tags = data_set.layer(self.layer_name)
        layer_seq = self.vocab.transform_and_pad(layer_tags, self.max_sentence_length)
        layer_y = [to_categorical(i, num_classes=self.vocab.size()) for i in layer_seq]
        return np.array(layer_y)
