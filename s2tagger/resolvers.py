import pandas as pd


class LexemeResolver:
    def __init__(self, file_name):
        self.lexeme_mapping = pd.read_csv(file_name, index_col=0)

    def resolve(self, lexeme_id):
        try:
            row = self.lexeme_mapping.loc[int(lexeme_id)]
            return row['nodes']
        except KeyError:
            return lexeme_id
        except ValueError:
            return lexeme_id
        except IndexError:
            return lexeme_id


class TemplateResolver:
    def __init__(self, file_name):
        self.template_mapping = pd.read_csv(file_name, index_col=0)

    def resolve(self, template_id):
        try:
            row = self.template_mapping.loc[int(template_id)]
            return row['syncat'] + ' : ' + row['amr']
        except KeyError:
            return template_id
        except ValueError:
            return template_id
        except IndexError:
            return template_id
