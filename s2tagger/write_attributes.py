from argparse import ArgumentParser
from os.path import join

from .tagger import Tagger


def write_attributes(file, sentence_ids, sentences, prediction):
    for sentence_id, sentence, tag_dist_seq in zip(sentence_ids, sentences, prediction.index_confidence_pairs):
        file.write('# ::id {}\n'.format(sentence_id))

        file.write('# ::lextags ')
        first_tag = True

        for token_idx, token in enumerate(sentence):
            tag_dist = tag_dist_seq[token_idx]
            for idx, c in tag_dist:
                if not first_tag:
                    file.write(' | ')
                first_tag = False

                tag = prediction.vocab.idx2tok(idx)
                file.write('{} {} {}'.format(token_idx, tag, c))

        file.write('\n\n')


def main():
    argparser = ArgumentParser()
    argparser.add_argument('--input-dir', type=str)
    argparser.add_argument('--output-dir', type=str)

    args = argparser.parse_args()

    tagger = Tagger.load(args.output_dir)
    input_data = InputDataSet(args.input_dir, val_split=0.8)
    sentences = input_data.val_data['sent']
    prediction = tagger.predict(sentences)['tmpl']
    prediction = prediction.clip_confidence(0.95)
    sent_ids = ['{}'.format(i) for i in range(len(sentences))]

    with open(join(args.output_dir, 'attr.txt'), 'w') as attr_file:
        write_attributes(attr_file, sent_ids, sentences, prediction)


if __name__ == '__main__':
    main()
