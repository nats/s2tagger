from tensorflow.keras import backend as K


def mean_confidence(y_true, y_pred):
    confidence = K.max(y_true * y_pred, axis=2)  # examples * tokens * predictions
    return K.mean(confidence)


def log_likelihood(y_true, y_pred):
    confidence = K.max(y_true * y_pred, axis=2)  # examples * tokens * predictions
    return K.sum(K.log(confidence))


def masked_mean(tensor, mask):
    """
    Calculate the mean only over the values for which the mask is 1.

    :param tensor: a tensor
    :param mask: the mask to apply, should contain 0 and 1 values
    :return: tensor, the masked mean
    """
    masked = tensor * mask
    summed = K.cast(K.sum(masked), 'float32')
    count = K.cast(K.maximum(K.sum(mask), 1), 'float32')
    return summed / count


def top_k_accuracy(k, mask_values=None, name=None):
    """
    Calculate the top-k masked accuracy.
    :param k: Number of top ranks to consider
    :param mask_values: Value to mask in score calculation. By default, 0 is masked.
    :return: metric function
    """
    if not mask_values:
        mask_values = [0]

    def topk(y_true, y_pred):
        s = K.shape(y_true)
        shape = (s[0] * s[1], s[2])
        y_true = K.reshape(y_true, shape)
        y_pred = K.reshape(y_pred, shape)
        class_id_true = K.argmax(y_true, axis=-1)

        # Create the combined mask by iterating over all mask values
        mask = K.cast(K.not_equal(class_id_true, mask_values[0]), 'int32')
        for mask_value in mask_values[1:]:
            other_mask = K.cast(K.not_equal(class_id_true, mask_value), 'int32')
            mask = K.minimum(mask, other_mask)  # AND the masks together

        correct = K.in_top_k(y_pred, K.argmax(y_true, axis=-1), k)
        correct = K.cast(correct, 'int32')
        return masked_mean(correct, mask)

    topk.__name__ = name if name else 'top{}'.format(k)
    return topk
