class Distribution:
    """
    Represents the distribution for a single tag prediction. Associates each predicted tag (given as an index into
    a vocabulary) with a confidence.
    """

    def __init__(self, indices, confidences):
        """
        Create a distribution.

        Both arguments must be of the same length and sorted by descending confidence.

        :param indices: list of tag indices
        :param confidences: list of confidence values
        """
        self.indices = indices
        self.confidences = confidences

    def index_confidence_pairs(self):
        """
        Return an iterable of pairs of (tag index, confidence).

        :return: iterable
        """
        return zip(self.indices, self.confidences)

    def top_n(self, n):
        """
        Limit the distribution to the top-n predictions.

        :param n: number of predictions to keep.
        :return: a Distribution
        """
        return Distribution(self.indices[:n], self.confidences[:n])

    def min_confidence(self, threshold):
        """
        Limit the distribution to predictions that have at least a given minimum confidence.

        Does not filter if no predictions are above the threshold.

        :param threshold: the threshold to enforce
        :return: a Distribution
        """
        filtered = [(idx, c) for idx, c in zip(self.indices, self.confidences) if c >= threshold]
        if not filtered:  # Do not filter if there are no matching entries
            filtered = zip(self.indices, self.confidences)
        f_indices, f_confidences = zip(*[(idx, c) for idx, c in filtered])
        return Distribution(f_indices, f_confidences)

    def clip_confidence(self, threshold):
        """
        Limit the distribution to the top predictions making up at a total confidence of 'threshold'.

        :param threshold: cumulative confidence to keep
        :return: a Distribution
        """
        i = 0
        acc = 0.0
        for c in self.confidences:
            acc += c
            i += 1
            if acc >= threshold:
                break
        return self.top_n(i)


class Prediction:
    """
    Represents predictions over a set of sentences.
    """

    def __init__(self, distribution_sequences, vocab, name=None):
        """
        Create a prediction.

        :param distribution_sequences: list of list of distributions, grouped by sentence
        :param vocab: vocabulary to resolve tag indices
        """
        self.distribution_sequences = distribution_sequences
        self.vocab = vocab
        self.name = name

    @staticmethod
    def from_model_prediction(y_pred, vocab, name=None):
        def mk_distribution(dist):
            tag_indices = (-dist).argsort()
            confidences = dist[tag_indices]
            return Distribution(tag_indices, confidences)

        distribution_sequences = [[mk_distribution(dist) for dist in seq] for seq in y_pred]
        return Prediction(distribution_sequences, vocab, name)

    def map(self, f):
        """
        Maps a function over each individual distribution.

        :param f: a function from a distribution to a distribution
        :return: a Prediction
        """
        return Prediction([[f(d) for d in seq] for seq in self.distribution_sequences], self.vocab, self.name)

    def indices(self):
        return [[d.indices for d in seq] for seq in self.distribution_sequences]

    def tags(self):
        return [[[self.vocab.idx2tok(idx) for idx in d.indices] for d in seq] for seq in self.distribution_sequences]

    def top_n(self, n):
        return self.map(lambda d: d.top_n(n))

    def min_confidence(self, threshold):
        """
        Enforce a minimum confidence. Discard all predictions below the threshold.

        :param threshold: threshold to enforce
        :return: a Prediction
        """
        return self.map(lambda d: d.min_confidence(threshold))

    def clip_confidence(self, threshold):
        """
        Enforce a cumulative confidence threshold. Discard all predictions not in the top X% of confidence mass.
        The returned prediction retains at least the 'threshold' portion of total prediction confidence.

        :param threshold: cumulative confidence mass to keep
        :return: a Prediction
        """
        return self.map(lambda d: d.clip_confidence(threshold))

    def val_ranks(self, val, exclude=None):
        if exclude is None:
            exclude = []

        def rank(ranked, val_tag):
            try:
                if val_tag == exclude:
                    return -1
                return ranked.index(self.vocab.tok2idx(val_tag))
            except ValueError:
                return -1

        def rank_seq(ranked_seq, val_seq):
            return [rank(ranked, val_tag) for ranked, val_tag in zip(ranked_seq, val_seq)]

        return [rank_seq(ranked_seq, val_seq) for ranked_seq, val_seq in zip(self.indices(), val)]
