from tensorflow.keras import backend as K


# https://stackoverflow.com/questions/47057361/keras-using-tensorflow-backend-masking-on-loss-function
def masked_categorical_crossentropy(mask_value):
    mask_value = K.variable(mask_value)

    def loss(y_true, y_pred):
        # find out which timesteps in `y_true` are not the padding character '#'
        mask = K.all(K.equal(y_true, mask_value), axis=-1)
        mask = 1 - K.cast(mask, K.floatx())

        # multiply categorical_crossentropy with the mask
        loss = K.categorical_crossentropy(y_true, y_pred) * mask

        # take average w.r.t. the number of unmasked entries
        return K.sum(loss) / K.sum(mask)

    return loss
