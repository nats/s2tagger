from argparse import ArgumentParser
from pathlib import Path

from s2tagger.annotate_data_set import annotate_data_set
from s2tagger.data_set import load_data_set
from s2tagger.jackknife import make_split
from s2tagger.tagger import Tagger
from s2tagger.tagger_config import TaggerConfig


def main():
    argparser = ArgumentParser()

    subparsers = argparser.add_subparsers()

    # train command

    train_cmd = subparsers.add_parser('train',
                                      help='Train the parser with the train data set in the config file.')
    train_cmd.set_defaults(mode='train')
    train_cmd.add_argument('--config', '-c', type=str, required=True,
                           help='Config file specifying the experiment')
    train_cmd.add_argument('--model', '-m', type=str,
                           help='Directory to save the model and other output in')
    train_cmd.add_argument('--val', '-v', type=float, default=.2,
                           help='Portion of the train data to use for validation')

    # predict command

    predict_cmd = subparsers.add_parser('predict',
                                        help='Load a trained parser and predict tags for a specified data set')
    predict_cmd.set_defaults(mode='predict')
    predict_cmd.add_argument('--config', '-c', type=str,
                             help='Config file specifying data sets. '
                                  'If omitted, the configuration from the saved model is used.')
    predict_cmd.add_argument('--model', '-m', type=str,
                             help='Directory to read the model from')
    predict_cmd.add_argument('--output', '-o', type=str, required=True,
                             help='File to write predicted tags to')
    predict_cmd.add_argument('--data-set', type=str, default='test',
                             help='Name of the data set (as specified in the config file) to predict tags for.')

    # jackknife command

    jackknife_cmd = subparsers.add_parser('jackknife',
                                          help='Make predictions on the training set using jackknifing')
    jackknife_cmd.set_defaults(mode='jackknife')
    jackknife_cmd.add_argument('--config', '-c', type=str,
                               help='Config file')
    jackknife_cmd.add_argument('--model', '-m', type=str,
                               help='Directory to save the models in. '
                                    'A numbered sub-directory will be created for each split')
    jackknife_cmd.add_argument('--output', '-o', type=str, required=True,
                               help='File to write predicted tags to')
    jackknife_cmd.add_argument('--splits', '-n', type=int, default=5,
                               help='Number of portions to separate the training data into')

    args = argparser.parse_args()

    if args.mode == 'train':
        train(args)
    elif args.mode == 'predict':
        predict(args)
    elif args.mode == 'jackknife':
        jackknife(args)


def train(args):
    config = TaggerConfig.load(args.config)

    train_data = load_data_set(config, 'train')

    val_data = None
    if args.val:
        print('Validating with {} of the train data'.format(args.val))
        train_data, val_data = train_data.split(1.0 - args.val)
    else:
        print('Not validating!')

    supertagger = Tagger(config)
    supertagger.train(train_data, val_data=val_data)
    if args.model:
        supertagger.save(args.model)

    return supertagger


def predict(args):
    tagger = Tagger.load(args.model)

    if args.config:
        config = TaggerConfig.load(args.config)
    else:
        config = tagger.config

    pred_data = load_data_set(config, args.data_set)

    predictions = tagger.predict(pred_data)

    pred_data = pred_data.bare()
    for p in predictions:
        pred_data = annotate_data_set(pred_data, [p], p.name)

    with open(args.output, 'w') as f:
        pred_data.save_attribute_file(f)


def jackknife(args):
    config = TaggerConfig.load(args.config)

    train_data = load_data_set(config, 'train')

    sections = train_data.n_way_split(args.splits)

    splits = [make_split(sections, i) for i in range(args.splits)]

    prediction_lists = []
    for i, (train_set, pred_set) in enumerate(splits):
        print(f'Jackknifing split {i+1}/{len(splits)}')
        supertagger = Tagger(config)
        supertagger.train(train_set, pred_set)
        prediction_list = supertagger.predict(pred_set)
        prediction_lists.append(prediction_list)
        if args.model:
            supertagger.save(Path(args.model) / str(i+1))
    prediction_layers = zip(*prediction_lists)

    pred_data = train_data.bare()
    for layer in prediction_layers:
        pred_data = annotate_data_set(pred_data, layer, layer[0].name)

    with open(args.output, 'w') as f:
        pred_data.save_attribute_file(f)


if __name__ == '__main__':
    main()
