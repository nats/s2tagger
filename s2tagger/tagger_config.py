import os
from typing import Optional

import yaml

from .tagger_inputs import GloveInput, FeatureInput, ElmoInput, TrainedEmbedding
from .tagger_output import TaggerOutput


class TaggerConfig:
    def __init__(self, config, file_name):
        self.config = config
        self.config_file_name = file_name

    def __getitem__(self, key, default=None):
        return self.config[key]

    def __contains__(self, key):
        return key in self.config

    def resolve_file(self, file_name):
        return os.path.join(os.path.dirname(self.config_file_name), file_name)

    def create_tagger_inputs(self, train_data):
        inputs = []

        def get_input_config(container, key) -> Optional[dict]:
            """Input configs can be a simple string or a dict with a single key.
            This function normalises the representation by always returning a (possibly empty) dict."""
            if key in container:  # string key exists, without further configuration
                return {}
            config = next((x for x in container if isinstance(x, dict) and key in x), None)
            return config

        trained_cfg = get_input_config(self['features']['word_embeddings'], 'trained')
        if trained_cfg is not None:
            min_count = trained_cfg.get('min_count', 0)
            dimension = trained_cfg.get('dimension', 100)
            inputs.append(TrainedEmbedding(train_data,
                                           max_sentence_length=self['max_sentence_length'],
                                           min_count=min_count,
                                           embedding_dim=dimension))

        glove_cfg = get_input_config(self['features']['word_embeddings'], 'glove')
        if glove_cfg is not None:
            inputs.append(GloveInput(self.resolve_file(self['glove_path']), self['max_sentence_length']))

        if 'elmo' in self['features']['word_embeddings']:
            inputs.append(ElmoInput(self['max_sentence_length']))

        for entry in self['features']['feature_layers']:
            if isinstance(entry, dict):
                layer, feature_cfg = next(iter(entry.items()))
            elif isinstance(entry, str):
                layer = entry
                feature_cfg = {}
            else:
                raise Exception(f'Unable to process feature layer {entry}')
            embedding_dim = feature_cfg.get('embedding_dim', 100)
            inputs.append(FeatureInput(train_data, layer, self['max_sentence_length'], embedding_dim))

        return inputs

    def create_tagger_outputs(self, train_data):
        outputs = []

        for entry in self['predict_layers']:
            if isinstance(entry, dict):
                layer, config = next(iter(entry.items()))
            elif isinstance(entry, str):
                layer = entry
                config = {}
            else:
                raise Exception(f'Unable to process predict layer {entry}')
            min_count = config.get("min_count", 2)
            outputs.append(TaggerOutput(train_data, layer, self['max_sentence_length'], min_count=min_count))

        return outputs

    def save(self, file_name):
        with open(file_name, 'w') as f:
            yaml.dump(self.config, f)

    @staticmethod
    def load(file_name):
        with open(file_name) as f:
            return TaggerConfig(yaml.load(f), file_name)

