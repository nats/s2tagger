
def format_distribution(distribution, vocab):
    return ','.join('{}|{}'.format(vocab.idx2tok(i), c) for i, c in distribution.index_confidence_pairs())


def format_prediction(prediction):
    return [[format_distribution(d, prediction.vocab) for d in ds] for ds in prediction.distribution_sequences]


def annotate_data_set(data_set, predictions, name=None):
    fmt_preds = [format_prediction(pred) for pred in predictions]
    fmt_tag_seqs = [tag_seq for tag_seqs in fmt_preds for tag_seq in tag_seqs]

    pred_data = data_set.add_layer(fmt_tag_seqs, name)
    return pred_data
