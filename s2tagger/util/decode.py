import re
from argparse import ArgumentParser
from pathlib import Path
from typing import TextIO, List, NamedTuple

import pandas as pd

from s2tagger.data_set import load_layer_file

Prediction = NamedTuple('Prediction', [('tag_id', str), ('probability', float)])


def parse_prediction(pred_str: str) -> Prediction:
    id_str, prop_str = pred_str.split('|')
    return Prediction(id_str, float(prop_str))


def main():
    argparser = ArgumentParser()

    argparser.add_argument('--sent', required=True,
                           help='path to a file containing sentence IDs and sentence tokens')
    argparser.add_argument('--lexDir', required=True,
                           help='path where lexicon data is saved')
    argparser.add_argument('--predictions', required=True,
                           help='path to predictions file')

    args = argparser.parse_args()

    decode(args.predictions, args.lexDir, args.sent)


def decode(pred_file_name: str, lex_dir: str, sentence_file_name: str):
    print(f'Loading lexicon from {lex_dir}')
    lexicon = pd.read_csv(Path(lex_dir) / 'tmpl_parameters.csv', index_col=0, dtype={'id': int})

    print(f'Loading sentences from {sentence_file_name}')
    with open(sentence_file_name) as sentence_file:
        examples = load_layer_file(sentence_file)

    print(f'Loading prediction file {pred_file_name}')
    with open(pred_file_name) as pred_file:
        sentence_predictions = load_prediction_file(pred_file)

    print()
    for example, token_predictions in zip(examples, sentence_predictions):
        print(f'================================================================')
        print(f'Sentence {example.id}')
        print(f'================================================================')
        print()
        for token, predictions in zip(example.sentence, token_predictions):
            print(token)
            print('----------------')
            for prediction in predictions:
                try:
                    tag_id = int(prediction.tag_id)
                    tag_row = lexicon.loc[tag_id]
                    print('{:0.3f} {:>7}  {} : {}'.format(prediction.probability, tag_id, tag_row['syncat'], tag_row['amr']))
                except ValueError:
                    print('{:0.3f} {:>7}'.format(prediction.probability, prediction.tag_id))
            print()
        print()


line_re = re.compile(r'# ::supertags-template-id (.*)')


def load_prediction_file(pred_file: TextIO) -> List[List[List[Prediction]]]:
    all_preds = []
    for line in pred_file.readlines():
        match = line_re.match(line)
        if match:
            attr_value = match.group(1)
            sentence_preds = [[parse_prediction(token_pred) for token_pred in token_preds.split(',')] for token_preds in attr_value.split(' ')]
            all_preds.append(sentence_preds)
    return all_preds


if __name__ == '__main__':
    main()
