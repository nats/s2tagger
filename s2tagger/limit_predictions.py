def limit(prediction, min_confidence=None, clip_confidence=None, max_predictions=None):
    if min_confidence:
        prediction = prediction.min_confidence(min_confidence)

    if clip_confidence:
        prediction = prediction.clip_confidence(clip_confidence)

    if max_predictions:
        prediction = prediction.top_n(max_predictions)
    return prediction
