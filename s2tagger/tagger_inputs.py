from tensorflow.keras import Input
from tensorflow.keras.layers import Embedding, Masking
import numpy as np
from tensorflow.keras.preprocessing.sequence import pad_sequences

from .vocabulary import GloveVocabulary, Vocabulary


class TrainedEmbedding:
    def __init__(self, data_set, max_sentence_length: int, embedding_dim: int, min_count: int):
        self.vocab = Vocabulary(data_set.sentences(), min_count=min_count)
        self.max_sentence_length = max_sentence_length
        self.embedding_dim = embedding_dim

    def embed(self):
        inp = Input(shape=(self.max_sentence_length,), name='word_input')
        embedded = Embedding(
            name='trained_embedding',
            input_dim=self.vocab.size(),
            output_dim=self.embedding_dim,
            input_length=self.max_sentence_length,
            mask_zero=True
        )(inp)

        return inp, embedded

    def extract(self, data_set):
        return self.vocab.transform_and_pad(data_set.sentences(), self.max_sentence_length)


class GloveInput:
    def __init__(self, glove_file: str, max_sentence_length: int):
        self.max_sentence_length = max_sentence_length

        self.word_vocab = GloveVocabulary.get(glove_file)

    def embed(self):
        embedding_matrix = self.word_vocab.embedding_matrix
        word_inp = Input(shape=(self.max_sentence_length,), name='glove_input')
        word_embedded = Embedding(
            name='glove_embedding',
            input_dim=embedding_matrix.shape[0],
            output_dim=embedding_matrix.shape[1],
            input_length=self.max_sentence_length,
            weights=[embedding_matrix],
            mask_zero=True
        )(word_inp)

        return word_inp, word_embedded

    def extract(self, data_set):
        return self.word_vocab.transform_and_pad(data_set.sentences(), self.max_sentence_length)


class FeatureInput:
    def __init__(self, data_set, layer_name: str, max_sentence_length: int, embedding_dim: int, min_count: int = 2):
        self.layer_name = layer_name
        self.max_sentence_length = max_sentence_length
        self.embedding_dim = embedding_dim
        self.min_count = min_count

        self.vocab = Vocabulary(data_set.layer(layer_name), min_count=min_count)

    def embed(self):
        feat_inp = Input(shape=(self.max_sentence_length,), name='{}_input'.format(self.layer_name))
        feat_embedded = Embedding(
            name='{}_embedding'.format(self.layer_name),
            input_dim=self.vocab.size(),
            output_dim=self.embedding_dim,
            input_length=self.max_sentence_length,
            mask_zero=True
        )(feat_inp)

        return feat_inp, feat_embedded

    def extract(self, data_set):
        layer_tags = data_set.layer(self.layer_name)
        layer_seq = self.vocab.transform_and_pad(layer_tags, self.max_sentence_length)
        return np.array(layer_seq)


class ElmoInput:
    def __init__(self, max_sentence_length: int):
        self.max_sentence_length = max_sentence_length

    def embed(self):
        inp = Input(shape=(self.max_sentence_length, 1024), name='elmo')
        masked = Masking()(inp)
        return inp, masked

    def extract(self, data_set):
        layer = data_set.layer('elmo')
        layer_padded = pad_sequences(layer, self.max_sentence_length, padding='post')
        return layer_padded
