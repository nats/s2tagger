"""
Annotates a training data file with predicted tags using jackknifing.

The training data is separated into n portions. A s2tagger is trained on every combination of (n-1) portions, and the
predictions for the nth respective portion are included in the output.
"""

from s2tagger.data_set import DataSet


def make_split(data_sets, split_idx):
    """
    Combines a portioned data set into train and prediction data.

    One of the given data sets is used for prediction, the rest for training.

    :param data_sets: a list of data sets
    :param split_idx: index of the data set to
    :return: a pair of train, pred data sets.
    """
    pred = data_sets[split_idx]
    train = DataSet.combine(data_sets[:split_idx] + data_sets[split_idx + 1:])
    return train, pred
